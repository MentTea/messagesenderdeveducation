# MessageSenderDevEducation

@authors: Grigory Ershov, Oleg Baranov, Ivan Tarasov.

Java advance course team project.
 This application is event-driven real-time pub/sub notification system, that relays messages to users 
 on different platforms, including Telegram, Skype and email notifications.

Technologies/Design Decisions

    Java 1.8 with Spring Boot.
    Database: MySql.
    JPA: Hibernate.
    Spring Data.
    Security: Spring Security.
    Spring Controllers implements REST API.
    Maven
    Lombok 

Features

    Application is adaptive, can be used in different scenarios, such as notifying users with local news etc.
    Users can set and modify their subscriptions.
    User data persists to local database, independent of outer service.
    Messages can be relayed to email.
    

    
    
    

