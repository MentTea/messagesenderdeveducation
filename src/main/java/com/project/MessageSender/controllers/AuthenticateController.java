package com.project.MessageSender.controllers;

import com.project.MessageSender.models.Admin;
import com.project.MessageSender.dto.AuthenticationRequestDTO;
import com.project.MessageSender.exceptions.security.JwtAuthenticationException;
import com.project.MessageSender.repositories.AdminRepository;
import com.project.MessageSender.security.jwt.JwtTokenProvider;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AuthenticateController {
  private final AuthenticationManager authenticationManager;

  private final JwtTokenProvider jwtTokenProvider;

  private final AdminRepository adminRepository;

  @Autowired
  public AuthenticateController(
      AuthenticationManager authenticationManager,
      JwtTokenProvider jwtTokenProvider,
      AdminRepository adminRepository) {
    this.authenticationManager = authenticationManager;
    this.jwtTokenProvider = jwtTokenProvider;
    this.adminRepository = adminRepository;
  }

  @PostMapping("/auth")
  public ResponseEntity login(@RequestBody AuthenticationRequestDTO requestDTO) {
    try {
      String adminEmail = requestDTO.getAdminEmail();
      String outerApi = requestDTO.getOuterApi();

      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(adminEmail, requestDTO.getPassword()));
      Admin admin = adminRepository.findAdminByEmail(requestDTO.getAdminEmail());

      if (admin == null) {
        throw new UsernameNotFoundException("Admin with email: " + adminEmail + " not found");
      }
      String token = jwtTokenProvider.createToken(adminEmail, outerApi);
      Map<Object, Object> response = new HashMap<>();

      response.put("token", token);
      return ResponseEntity.ok(response);
    } catch (JwtAuthenticationException e) {
      throw new BadCredentialsException("Invalid admin email or password");
    }
  }
}
