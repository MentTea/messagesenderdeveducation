package com.project.MessageSender.controllers;

import com.project.MessageSender.dto.UserCreateDto;
import com.project.MessageSender.dto.UserUpdateDto;
import com.project.MessageSender.security.jwt.JwtTokenProvider;
import com.project.MessageSender.services.UserService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user")
public class UserController {

  private final JwtTokenProvider jwtTokenProvider;
  private final UserService userService;

  @PostMapping
  @ResponseStatus(HttpStatus.OK)
  public void createUser(@Valid @RequestBody UserCreateDto userCreateDto,
      @RequestHeader(name = "Authorization") String bearerToken) {
    String outerApiName = jwtTokenProvider
        .getOuterApi(jwtTokenProvider.getTokenFromBearerToken(bearerToken));

    userService.addUser(userCreateDto, outerApiName);
  }

  @PutMapping(value = "/{email}")
  @ResponseStatus(HttpStatus.OK)
  public void updateUser(@Valid @RequestBody UserUpdateDto userUpdateDto,
      @PathVariable String email, @RequestHeader(name = "Authorization") String bearerToken) {
    String outerApiName = jwtTokenProvider
        .getOuterApi(jwtTokenProvider.getTokenFromBearerToken(bearerToken));

    userService.updateUser(email, userUpdateDto, outerApiName);
  }

  @DeleteMapping(value = "/{email}")
  @ResponseStatus(HttpStatus.OK)
  public void deleteUser(@PathVariable String email) {
    userService.deleteUser(email);
  }
}
