package com.project.MessageSender.controllers;

import com.project.MessageSender.dto.MessageDto;
import com.project.MessageSender.security.jwt.JwtTokenProvider;
import com.project.MessageSender.services.MessengersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/messenger")
public class MessageController {

  @Autowired
  private MessengersService messengersService;

  @Autowired
  private JwtTokenProvider jwtTokenProvider;

  @PostMapping("/sendMessage")
  @ResponseStatus(HttpStatus.OK)
  public void sendMessage(@RequestBody MessageDto message,
      @RequestHeader(name = "Authorization") String bearerToken) {
    String outerApi = jwtTokenProvider
        .getOuterApi(jwtTokenProvider.getTokenFromBearerToken(bearerToken));
    messengersService.sendMessage(message, outerApi);
  }
}
