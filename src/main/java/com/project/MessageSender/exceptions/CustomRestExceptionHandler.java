package com.project.MessageSender.exceptions;

import java.time.LocalDateTime;
import javax.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomRestExceptionHandler {

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ApiError> entityNotFoundHandler(EntityNotFoundException ex) {
    ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getMessage(), LocalDateTime.now());
    return new ResponseEntity<>(apiError, apiError.getStatus());
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ApiError> methodArgumentNotValidHandler(
      MethodArgumentNotValidException ex) {
    ValidationError validationError = new ValidationError(ex.getBindingResult().getFieldErrors());
    return new ResponseEntity<>(validationError, validationError.getStatus());
  }

  @ExceptionHandler(AlreadyExistException.class)
  public ResponseEntity<ApiError> entityAlreadyExistHandler(AlreadyExistException ex) {
    ApiError apiError = new ApiError(HttpStatus.CONFLICT, ex.getMessage(), LocalDateTime.now());
    return new ResponseEntity<>(apiError, apiError.getStatus());
  }

  @ExceptionHandler(InvalidException.class)
  public ResponseEntity<ApiError> entityInvalidHandler(InvalidException ex) {
    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), LocalDateTime.now());
    return new ResponseEntity<>(apiError, apiError.getStatus());
  }

  @ExceptionHandler(LogicalException.class)
  public ResponseEntity<ApiError> entityInvalidHandler(LogicalException ex) {
    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), LocalDateTime.now());
    return new ResponseEntity<>(apiError, apiError.getStatus());
  }

}

