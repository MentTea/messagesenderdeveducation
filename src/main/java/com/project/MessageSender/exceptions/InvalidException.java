package com.project.MessageSender.exceptions;

public class InvalidException extends RuntimeException {

  public InvalidException(String message) {
    super(message);
  }
}
