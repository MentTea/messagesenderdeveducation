package com.project.MessageSender.exceptions;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@Getter
public class ApiError {

  private HttpStatus status;
  private String message;
  private LocalDateTime timestamp;

  public ApiError(HttpStatus httpStatus, String message) {
    this(httpStatus, message, LocalDateTime.now());
  }

}
