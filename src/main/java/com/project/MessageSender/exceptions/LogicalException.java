package com.project.MessageSender.exceptions;

public class LogicalException extends RuntimeException {

  public LogicalException(String message) {
    super(message);
  }
}
