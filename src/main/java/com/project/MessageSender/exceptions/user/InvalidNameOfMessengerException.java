package com.project.MessageSender.exceptions.user;

import com.project.MessageSender.exceptions.InvalidException;

public class InvalidNameOfMessengerException extends InvalidException {

  public InvalidNameOfMessengerException() {
    super("Invalid JSON request!"
        + " Unreal name of the messenger!");
  }
}
