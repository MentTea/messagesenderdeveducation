package com.project.MessageSender.exceptions.user;

import javax.persistence.EntityNotFoundException;

public class UserNotFoundException extends EntityNotFoundException {

  public UserNotFoundException(String message) {
    super("No matching user found with @email = '"
        + message
        + "'!");
  }
}
