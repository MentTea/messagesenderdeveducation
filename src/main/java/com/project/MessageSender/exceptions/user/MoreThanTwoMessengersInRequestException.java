package com.project.MessageSender.exceptions.user;

import com.project.MessageSender.exceptions.LogicalException;

public class MoreThanTwoMessengersInRequestException extends LogicalException {

  public MoreThanTwoMessengersInRequestException(int count) {
    super("Invalid JSON request!"
        + " Expected number of arguments 2, but was '"
        + count
        + "'!");
  }
}
