package com.project.MessageSender.exceptions.user;

import com.project.MessageSender.exceptions.LogicalException;

public class DuplicateMessengerNameInTheRequestException extends LogicalException {

  public DuplicateMessengerNameInTheRequestException(String firstName, String secondName) {
    super("Invalid JSON request!"
        + " Messengers names are identical!"
        + " '"
        + firstName
        + "' and '"
        + secondName
        + "'!");
  }
}
