package com.project.MessageSender.exceptions.user;

import com.project.MessageSender.exceptions.InvalidException;

public class InvalidEmailException extends InvalidException {

  public InvalidEmailException(String message) {
    super(message);
  }
}
