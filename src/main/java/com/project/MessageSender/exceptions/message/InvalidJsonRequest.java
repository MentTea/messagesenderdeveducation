package com.project.MessageSender.exceptions.message;

import com.project.MessageSender.exceptions.InvalidException;

public class InvalidJsonRequest extends InvalidException {

  public InvalidJsonRequest(String name) {
    super("Invalid JSON request,field " + name + " should not be null");
  }


}
