package com.project.MessageSender.exceptions.message;

import com.project.MessageSender.exceptions.InvalidException;

public class InvalidScheduleTimeFormat extends InvalidException {

  public InvalidScheduleTimeFormat(String message) {
    super(message);
  }
}
