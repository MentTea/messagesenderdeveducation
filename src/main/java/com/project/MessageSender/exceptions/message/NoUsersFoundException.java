package com.project.MessageSender.exceptions.message;

public class NoUsersFoundException extends RuntimeException {

  public NoUsersFoundException(String message) {
    super(message);
  }
}
