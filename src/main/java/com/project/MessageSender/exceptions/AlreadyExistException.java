package com.project.MessageSender.exceptions;

public class AlreadyExistException extends RuntimeException {

  public AlreadyExistException(String message) {
    super(message);
  }
}
