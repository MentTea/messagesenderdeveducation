package com.project.MessageSender.repositories;

import com.project.MessageSender.models.OuterApi;
import com.project.MessageSender.models.User;
import com.project.MessageSender.models.UserSubscriptions;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserSubscriptionsRepository extends JpaRepository<UserSubscriptions, Long> {

  List<UserSubscriptions> findAllByUser(User user);

  List<UserSubscriptions> findAllByOuterApiAndSubscribeIsTrueAndTypeNewsIsTrue(OuterApi api);

  List<UserSubscriptions> findAllByOuterApiAndSubscribeIsTrueAndTypeErrorIsTrue(OuterApi api);

  List<UserSubscriptions> findAllByOuterApiAndSubscribeIsTrueAndTypeWarningIsTrue(OuterApi api);

  List<UserSubscriptions> findAllByOuterApiAndSubscribeIsTrueAndTypeUpdateIsTrue(OuterApi api);

  List<UserSubscriptions> findAllByOuterApiAndSubscribeIsTrue(OuterApi api);
}
