package com.project.MessageSender.repositories;

import com.project.MessageSender.models.Messenger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessengerRepository extends JpaRepository<Messenger, String> {

  Messenger findByName(String name);
}
