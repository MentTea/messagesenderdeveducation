package com.project.MessageSender.repositories;

import com.project.MessageSender.models.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

  Optional<User> findByEmail(String email);

  boolean existsByTelegram(String telegram);

  boolean existsByEmail(String email);
}