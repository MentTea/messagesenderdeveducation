package com.project.MessageSender.repositories;

import com.project.MessageSender.models.OuterApi;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OuterApiRepository extends JpaRepository<OuterApi, String> {

  OuterApi findByName(String name);
}
