package com.project.MessageSender.repositories;

import com.project.MessageSender.models.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository  extends JpaRepository<Admin, String> {
  Admin findAdminByEmail(String email);
}
