package com.project.MessageSender.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.Data;


@Entity
@Table(name = "admins")
@Data
public class Admin {

  @Id
  @NotBlank
  @Column(name = "email", unique = true)
  private String email;

  @NotBlank
  @Column(name = "password")
  private String password;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinTable(
      name = "admin_outer_api",
      joinColumns = @JoinColumn(name = "admin_email"),
      inverseJoinColumns = @JoinColumn(name = "outer_api_name"))
  private OuterApi outerApi;
}
