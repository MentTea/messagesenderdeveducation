package com.project.MessageSender.models;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {

  @Id
  @NotBlank
  @Column(name = "email", unique = true)
  private String email;

  @Column(name = "telegram", unique = true)
  private String telegram;

  @OneToOne
  @JoinTable(
      name = "user_outer_api",
      joinColumns = @JoinColumn(name = "user_email"),
      inverseJoinColumns = @JoinColumn(name = "outer_api_name"))
  private OuterApi apiModel;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return email.equals(user.email) &&
        apiModel.equals(user.apiModel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, apiModel);
  }
}
