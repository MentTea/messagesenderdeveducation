package com.project.MessageSender.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_subscriptions")
public class UserSubscriptions {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "subscribe")
  private Boolean subscribe;

  @Column(name = "type_news")
  private Boolean typeNews;

  @Column(name = "type_error")
  private Boolean typeError;

  @Column(name = "type_warning")
  private Boolean typeWarning;

  @Column(name = "type_update")
  private Boolean typeUpdate;

  @ManyToOne
  @JoinColumn(name = "messenger_name")
  private Messenger messenger;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "user")
  private User user;

  @ManyToOne
  @JoinColumn(name = "outer_api_name")
  private OuterApi outerApi;
}
