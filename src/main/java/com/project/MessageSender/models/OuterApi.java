package com.project.MessageSender.models;

import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Entity
@Table(name = "outer_api")
@Data
public class OuterApi {

  @Id
  @NotBlank
  @Column(name = "name", unique = true)
  private String name;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "user_outer_api",
      joinColumns = @JoinColumn(name = "outer_api_name"),
      inverseJoinColumns = @JoinColumn(name = "user_email"))
  private Set<User> users;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OuterApi outerApi = (OuterApi) o;
    return name.equals(outerApi.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }
}
