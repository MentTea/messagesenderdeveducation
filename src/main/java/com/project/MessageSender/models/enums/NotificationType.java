package com.project.MessageSender.models.enums;

public enum NotificationType {
  EVENT,
  NEWS,
  ERROR,
  WARNING,
  UPDATE
}
