package com.project.MessageSender.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Entity
@Table(name = "messenger")
@Data
public class Messenger {

  @Id
  @NotBlank
  @Column(name = "name", unique = true)
  private String name;

  @Override
  public String toString(){
    return name;
  }
}