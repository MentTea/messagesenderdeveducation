package com.project.MessageSender.dto;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserUpdateDto {

  private String telegram;

  private List<UserSubscriptionsDto> userSubscriptionsDtoList;

}
