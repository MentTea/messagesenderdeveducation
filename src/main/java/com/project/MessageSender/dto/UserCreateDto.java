package com.project.MessageSender.dto;

import java.util.List;
import javax.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserCreateDto {

  @NotBlank
  private String email;

  private String telegram;

  private List<UserSubscriptionsDto> userSubscriptionsDtoList;
}
