package com.project.MessageSender.dto;

import javax.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserSubscriptionsDto {

  @NotBlank
  private String name;

  private Boolean subscribe;

  private Boolean news;

  private Boolean error;

  private Boolean warning;

  private Boolean update;

}
