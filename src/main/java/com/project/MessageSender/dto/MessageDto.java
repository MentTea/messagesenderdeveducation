package com.project.MessageSender.dto;

import com.project.MessageSender.models.enums.NotificationType;
import javax.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class MessageDto {

  @NotBlank
  private NotificationType type;
  @NotBlank
  private String messageBody;

  private String dateAndTimeScheduled;

  private Byte timezone;

}
