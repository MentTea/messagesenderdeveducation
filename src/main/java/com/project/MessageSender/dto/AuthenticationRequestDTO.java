package com.project.MessageSender.dto;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class AuthenticationRequestDTO {

  @NotBlank
  private String outerApi;

  @NotBlank
  private String adminEmail;

  @NotBlank
  private String password;
}
