package com.project.MessageSender.security.jwt;

import com.project.MessageSender.models.Admin;

public final class JwtAdminFactory {

  public JwtAdminFactory() {
  }

  public static JwtAdmin create(Admin admin) {
    return new JwtAdmin(
        admin.getEmail(),
        admin.getPassword(),
        admin.getOuterApi()
    );
  }
}
