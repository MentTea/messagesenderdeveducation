package com.project.MessageSender.security.jwt;

import com.project.MessageSender.models.OuterApi;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class JwtAdmin implements UserDetails {

  private final String email;
  private final String password;
  private final OuterApi outerApi;

  public JwtAdmin(String email, String password,
      OuterApi outerApi) {
    this.email = email;
    this.password = password;
    this.outerApi = outerApi;
  }

  public OuterApi getOuterApi() {
    return outerApi;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return null;
  }

  @JsonIgnore
  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getUsername() {
    return email;
  }

  @JsonIgnore
  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @JsonIgnore
  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @JsonIgnore
  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }
}
