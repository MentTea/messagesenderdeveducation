package com.project.MessageSender.security.jwt;

import com.project.MessageSender.models.Admin;
import com.project.MessageSender.repositories.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {
  private final AdminRepository adminRepository;

  @Autowired
  public JwtUserDetailsService(
      AdminRepository adminRepository) {
    this.adminRepository = adminRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String adminEmail) throws UsernameNotFoundException {
    Admin admin = adminRepository.findAdminByEmail(adminEmail);

    if (admin == null) {
      throw new UsernameNotFoundException("Admin with email: " + adminEmail + " not found");
    }
    return JwtAdminFactory.create(admin);
  }
}
