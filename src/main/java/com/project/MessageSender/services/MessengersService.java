package com.project.MessageSender.services;

import com.project.MessageSender.dto.MessageDto;

public interface MessengersService {

    void sendMessage(MessageDto messageDto, String outerApi);
}
