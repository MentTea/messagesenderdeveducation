package com.project.MessageSender.services;

import com.project.MessageSender.dto.UserCreateDto;
import com.project.MessageSender.dto.UserUpdateDto;

public interface UserService {

  void addUser(UserCreateDto userCreateDto, String outerApi);

  void updateUser(String email, UserUpdateDto userUpdateDto, String outerApi);

  void deleteUser(String email);
}
