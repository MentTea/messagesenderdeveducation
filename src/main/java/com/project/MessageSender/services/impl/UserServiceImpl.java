package com.project.MessageSender.services.impl;

import com.project.MessageSender.models.User;
import com.project.MessageSender.converters.DefaultUserSubscriptionsDtoConverter;
import com.project.MessageSender.dto.UserCreateDto;
import com.project.MessageSender.dto.UserUpdateDto;
import com.project.MessageSender.exceptions.AlreadyExistException;
import com.project.MessageSender.exceptions.email.EmailValidator;
import com.project.MessageSender.exceptions.user.DuplicateMessengerNameInTheRequestException;
import com.project.MessageSender.exceptions.user.InvalidEmailException;
import com.project.MessageSender.exceptions.user.InvalidNameOfMessengerException;
import com.project.MessageSender.exceptions.user.MoreThanTwoMessengersInRequestException;
import com.project.MessageSender.exceptions.user.UserNotFoundException;
import com.project.MessageSender.models.UserSubscriptions;
import com.project.MessageSender.repositories.MessengerRepository;
import com.project.MessageSender.repositories.OuterApiRepository;
import com.project.MessageSender.repositories.UserRepository;
import com.project.MessageSender.repositories.UserSubscriptionsRepository;
import com.project.MessageSender.services.UserService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final ConversionService conversionService;
  private final DefaultUserSubscriptionsDtoConverter defaultUserSubscriptionsDtoConverter;
  private final UserSubscriptionsRepository userSubscriptionsRepository;
  private final MessengerRepository messengerRepository;
  private final OuterApiRepository outerApiRepository;
  private final UserRepository userRepository;
  private final EmailValidator emailValidator;

  @Override
  public void addUser(UserCreateDto userCreateDto, String outerApi) {

    if (!emailValidator.validate(userCreateDto.getEmail())) {
      throw new InvalidEmailException("Not real Email address!");
    } else {
      if (userRepository.existsByEmail(userCreateDto.getEmail())) {
        throw new AlreadyExistException("A user with this Email address already exist!");
      } else {
        User user = User.builder()
            .email(userCreateDto.getEmail())
            .apiModel(outerApiRepository.findByName(outerApi))
            .build();

        if (userCreateDto.getTelegram() != null) {
          if (userRepository.existsByTelegram(userCreateDto.getTelegram())) {
            throw new AlreadyExistException("A user with this Telegram address already exist!");
          } else {
            user.setTelegram(userCreateDto.getTelegram());
          }
        }
        checkIsUserSubscriptionsDto(userCreateDto, user, outerApi);

        userRepository.save(user);
      }
    }
  }

  private void checkIsUserSubscriptionsDto(UserCreateDto userCreateDto, User user, String outerApi) {
    if (userCreateDto.getUserSubscriptionsDtoList() != null) {
      List<UserSubscriptions> userSubscriptions = userCreateDto.getUserSubscriptionsDtoList()
          .stream()
          .map(userSubscriptionsDto -> conversionService
              .convert(userSubscriptionsDto, UserSubscriptions.class))
          .collect(Collectors.toList());

      checkNumberOfDtoAndSaveInTheTable(user, userSubscriptions, outerApi);
    } else {
      UserSubscriptions userSubscriptions = defaultUserSubscriptionsDtoConverter.defaultConverter();
      userSubscriptions.setUser(user);
      userSubscriptions.setOuterApi(outerApiRepository.findByName(outerApi));
      userSubscriptionsRepository.save(userSubscriptions);
    }
  }

  private void checkNumberOfDtoAndSaveInTheTable(User user, List<UserSubscriptions> userSubscriptions, String outerApi) {
    switch (userSubscriptions.size()) {
      case 1:
        if (userSubscriptions.get(0).getMessenger() == null || messengerRepository.findByName(userSubscriptions.get(0).getMessenger().toString()) == null) {
          throw new InvalidNameOfMessengerException();
        } else {
          UserSubscriptions oneAdd = userSubscriptions.get(0);

          oneAdd.setUser(user);
          oneAdd.setOuterApi(outerApiRepository.findByName(outerApi));

          userSubscriptionsRepository.save(oneAdd);
        }
        break;
      case 2:
        if (userSubscriptions.get(0).getMessenger() == null || messengerRepository.findByName(userSubscriptions.get(0).getMessenger().toString()) == null) {
          throw new InvalidNameOfMessengerException();
        } else {
          if (userSubscriptions.get(1).getMessenger() == null || messengerRepository.findByName(userSubscriptions.get(1).getMessenger().toString()) == null) {
            throw new InvalidNameOfMessengerException();
          } else {
            if (userSubscriptions.get(0).getMessenger() == userSubscriptions.get(1).getMessenger()) {
              throw new DuplicateMessengerNameInTheRequestException(
                  userSubscriptions.get(0).getMessenger().toString(),
                  userSubscriptions.get(1).getMessenger().toString());
            } else {
              for (UserSubscriptions toAdd : userSubscriptions) {
                toAdd.setUser(user);
                toAdd.setOuterApi(outerApiRepository.findByName(outerApi));

                userSubscriptionsRepository.save(toAdd);
              }
            }
          }
        }
        break;
      default:
        throw new MoreThanTwoMessengersInRequestException(userSubscriptions.size());
    }
  }

  @Override
  public void updateUser(String email, UserUpdateDto userUpdateDto, String outerApi) {
    User user = userRepository.findByEmail(email).orElseThrow(() -> new UserNotFoundException(email));
    List<UserSubscriptions> userSubscriptionsFromTable = userSubscriptionsRepository.findAllByUser(user);

    if (userUpdateDto.getTelegram() != null) {
      if (userRepository.existsByTelegram(userUpdateDto.getTelegram())) {
        throw new AlreadyExistException("A user with this Telegram address already exist!");
      } else {
        user.setTelegram(userUpdateDto.getTelegram());
      }
    }

    if (userUpdateDto.getUserSubscriptionsDtoList() != null) {
      List<UserSubscriptions> userSubscriptions = userUpdateDto.getUserSubscriptionsDtoList()
          .stream()
          .map(userSubscriptionsDto -> conversionService.convert(userSubscriptionsDto, UserSubscriptions.class))
          .collect(Collectors.toList());

      switch (userSubscriptions.size()) {
        case 1:
          if (userSubscriptions.get(0).getMessenger() == null || messengerRepository.findByName(userSubscriptions.get(0).getMessenger().toString()) == null) {
            throw new InvalidNameOfMessengerException();
          } else {
            if (userSubscriptions.get(0).getMessenger().toString().equals("mail")) {
              setIfExist(userSubscriptions.get(0), userSubscriptionsFromTable.get(0));

              userSubscriptionsRepository.save(userSubscriptionsFromTable.get(0));
            } else {
              setIfExist(userSubscriptions.get(0), userSubscriptionsFromTable.get(1));

              userSubscriptionsRepository.save(userSubscriptionsFromTable.get(1));
            }
          }
          break;
        case 2:
          if (userSubscriptions.get(0).getMessenger() == null || messengerRepository.findByName(userSubscriptions.get(0).getMessenger().toString()) == null) {
            throw new InvalidNameOfMessengerException();
          } else {
            if (userSubscriptions.get(1).getMessenger() == null || messengerRepository.findByName(userSubscriptions.get(1).getMessenger().toString()) == null) {
              throw new InvalidNameOfMessengerException();
            } else {
              if (userSubscriptions.get(0).getMessenger() == userSubscriptions.get(1).getMessenger()) {
                throw new DuplicateMessengerNameInTheRequestException(
                    userSubscriptions.get(0).getMessenger().toString(),
                    userSubscriptions.get(1).getMessenger().toString());
              } else {
                if (userSubscriptions.get(0).getMessenger().toString().equals("mail")) {
                  setIfExist(userSubscriptions.get(0), userSubscriptionsFromTable.get(0));

                  userSubscriptionsRepository.save(userSubscriptionsFromTable.get(0));
                }

                if (userSubscriptionsFromTable.size() == 2) {
                  setIfExist(userSubscriptions.get(1), userSubscriptionsFromTable.get(1));

                  userSubscriptionsRepository.save(userSubscriptionsFromTable.get(1));
                } else {
                  UserSubscriptions ifNotExist = new UserSubscriptions();

                  setIfExist(userSubscriptions.get(1), ifNotExist);

                  if (ifNotExist.getSubscribe() != null) {
                    ifNotExist.setSubscribe(true);
                  }

                  if (ifNotExist.getTypeError() != null) {
                    ifNotExist.setTypeError(false);
                  }

                  if (ifNotExist.getTypeNews() != null) {
                    ifNotExist.setTypeNews(false);
                  }

                  if (ifNotExist.getTypeUpdate() != null) {
                    ifNotExist.setTypeUpdate(false);
                  }

                  if (ifNotExist.getTypeWarning() != null) {
                    ifNotExist.setTypeWarning(false);
                  }
                  ifNotExist.setMessenger(messengerRepository.findByName(userSubscriptions.get(1).getMessenger().toString()));
                  ifNotExist.setUser(user);
                  ifNotExist.setOuterApi(outerApiRepository.findByName(outerApi));

                  userSubscriptionsRepository.save(ifNotExist);
                }
              }
            }
          }
          break;
        default:
          throw new MoreThanTwoMessengersInRequestException(userSubscriptions.size());
      }
    }

    userRepository.save(user);
  }

  private void setIfExist(UserSubscriptions fromDto, UserSubscriptions fromTable) {
    if (fromDto.getSubscribe() != null) {
      fromTable.setSubscribe(fromDto.getSubscribe());
    }

    if (fromDto.getTypeError() != null) {
      fromTable.setTypeError(fromDto.getTypeError());
    }

    if (fromDto.getTypeNews() != null) {
      fromTable.setTypeNews(fromDto.getTypeNews());
    }

    if (fromDto.getTypeUpdate() != null) {
      fromTable.setTypeUpdate(fromDto.getTypeUpdate());
    }

    if (fromDto.getTypeWarning() != null) {
      fromTable.setTypeWarning(fromDto.getTypeWarning());
    }
  }

  @Override
  public void deleteUser(String email) {
    User user = userRepository.findByEmail(email).orElseThrow(() -> new UserNotFoundException(email));
    List<UserSubscriptions> userSubscriptionsFromTable = userSubscriptionsRepository.findAllByUser(user);

    for (UserSubscriptions toDelete : userSubscriptionsFromTable) {
      toDelete.setSubscribe(false);
      userSubscriptionsRepository.save(toDelete);
    }
  }
}
