package com.project.MessageSender.services.impl;

import com.project.MessageSender.dto.MessageDto;
import com.project.MessageSender.repositories.OuterApiRepository;
import com.project.MessageSender.repositories.UserSubscriptionsRepository;
import com.project.MessageSender.services.MessengersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MessengersServiceImpl implements MessengersService {

  private final MailSenderImpl mailSender;
  private final TelegramSenderImpl telegramSender;
  private final UserSubscriptionsRepository userSubscriptionsRepository;
  private final OuterApiRepository outerApiRepository;

  @Override
  public void sendMessage(MessageDto messageDto, String outerApi) {
    Thread messageThread = new SendMessageThreadImpl(userSubscriptionsRepository,
        outerApiRepository, telegramSender, mailSender, messageDto, outerApi);
    messageThread.start();
  }
}
