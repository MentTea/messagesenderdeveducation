package com.project.MessageSender.services.impl;

import com.project.MessageSender.dto.MessageDto;
import com.project.MessageSender.exceptions.message.InvalidScheduleTimeFormat;
import com.project.MessageSender.exceptions.message.InvalidJsonRequest;
import com.project.MessageSender.exceptions.message.NoUsersFoundException;
import com.project.MessageSender.models.UserSubscriptions;
import com.project.MessageSender.models.enums.NotificationType;
import com.project.MessageSender.repositories.OuterApiRepository;
import com.project.MessageSender.repositories.UserSubscriptionsRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SendMessageThreadImpl extends Thread {

  private final UserSubscriptionsRepository userSubscriptionsRepository;
  private final OuterApiRepository outerApiRepository;
  private final TelegramSenderImpl telegramSender;
  private final MailSenderImpl mailSender;
  private final MessageDto messageDto;
  private final String outerApi;

  @Override
  public void run() {
    checkDtoValidity(messageDto);
    List<UserSubscriptions> userSubscriptionsList = getTargetSubscriptions(messageDto.getType(),
        outerApi);

    if (userSubscriptionsList.isEmpty()) {
      throw new NoUsersFoundException("No users subscribed to current message type were found");
    }

    if (messageDto.getDateAndTimeScheduled() != null
        && !messageDto.getDateAndTimeScheduled().equals("")
        && messageDto.getTimezone() != null) {
      try {
        Thread.sleep(getTimeScheduled(messageDto));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    send(messageDto, userSubscriptionsList);
  }

  private void send(MessageDto messageDto, List<UserSubscriptions> userSubscriptionsList) {
    for (UserSubscriptions userSubscription : userSubscriptionsList) {
      if (userSubscription.getMessenger().getName().equals("mail")) {
        mailSender
            .sendMessage(userSubscription.getUser().getEmail(), outerApi + ": " +
                    messageDto.getType().toString(),
                messageDto.getMessageBody());

      } else if (userSubscription.getMessenger().getName().equals("telegram")) {
        telegramSender
            .sendMessage(userSubscription.getUser().getEmail(),
                messageDto.getType().toString(),
                messageDto.getMessageBody());
      }
    }
  }

  private List<UserSubscriptions> getTargetSubscriptions(NotificationType type, String outerApi) {

    if (type.equals(NotificationType.ERROR)) {
      return userSubscriptionsRepository.
          findAllByOuterApiAndSubscribeIsTrueAndTypeErrorIsTrue(
              outerApiRepository.findByName(outerApi));
    } else if (type.equals(NotificationType.NEWS)) {
      return userSubscriptionsRepository.
          findAllByOuterApiAndSubscribeIsTrueAndTypeNewsIsTrue(
              outerApiRepository.findByName(outerApi));
    } else if (type.equals(NotificationType.UPDATE)) {
      return userSubscriptionsRepository.
          findAllByOuterApiAndSubscribeIsTrueAndTypeUpdateIsTrue(
              outerApiRepository.findByName(outerApi));
    } else if (type.equals(NotificationType.WARNING)) {
      return userSubscriptionsRepository.
          findAllByOuterApiAndSubscribeIsTrueAndTypeWarningIsTrue(
              outerApiRepository.findByName(outerApi));
    } else {
      return userSubscriptionsRepository
          .findAllByOuterApiAndSubscribeIsTrue(outerApiRepository.findByName(outerApi));
    }
  }

  private void checkDtoValidity(MessageDto messageDto) {

    if (messageDto.getType() == null) {
      throw new InvalidJsonRequest("type");
    }
    if (messageDto.getMessageBody() == null) {
      throw new InvalidJsonRequest("message_body");
    }
  }

  private long getTimeScheduled(MessageDto messageDto) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      long timeZoneFromDto = messageDto.getTimezone() * 3600000;
      long grinchDefaultTimeZoneOfServer = TimeZone.getDefault().getRawOffset();

      long missingTime = 0;
      if (timeZoneFromDto > grinchDefaultTimeZoneOfServer) {
        missingTime = timeZoneFromDto - grinchDefaultTimeZoneOfServer;
      } else if (timeZoneFromDto < grinchDefaultTimeZoneOfServer) {
        missingTime = grinchDefaultTimeZoneOfServer - timeZoneFromDto;
      }
      long dtoTime = simpleDateFormat.parse(messageDto.getDateAndTimeScheduled()).getTime();
      long localTime = System.currentTimeMillis();

      if (timeZoneFromDto > grinchDefaultTimeZoneOfServer) {
        long neededTime = localTime + missingTime;
        return dtoTime - neededTime;
      } else if (timeZoneFromDto < grinchDefaultTimeZoneOfServer) {
        long neededTime = localTime - missingTime;
        return dtoTime - neededTime;
      } else {
        return dtoTime - localTime;
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }
    throw new InvalidScheduleTimeFormat("Invalid time format, use 'yyyy-MM-dd HH:mm:ss' pattern.");
  }
}
