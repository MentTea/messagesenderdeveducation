package com.project.MessageSender.converters;

import com.project.MessageSender.dto.UserSubscriptionsDto;
import com.project.MessageSender.models.UserSubscriptions;
import com.project.MessageSender.repositories.MessengerRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Data
@Component
@RequiredArgsConstructor
public class UserSubscriptionsDtoConverter implements Converter<UserSubscriptionsDto, UserSubscriptions> {

  private final MessengerRepository messengerRepository;

  @Override
  public UserSubscriptions convert(UserSubscriptionsDto userSubscriptionsDto) {
    return UserSubscriptions.builder()
        .subscribe(userSubscriptionsDto.getSubscribe())
        .typeNews(userSubscriptionsDto.getNews())
        .typeError(userSubscriptionsDto.getError())
        .typeUpdate(userSubscriptionsDto.getUpdate())
        .typeWarning(userSubscriptionsDto.getWarning())
        .messenger(messengerRepository.findByName(userSubscriptionsDto.getName()))
        .build();
  }
}
