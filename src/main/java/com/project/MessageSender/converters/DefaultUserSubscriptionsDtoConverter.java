package com.project.MessageSender.converters;

import com.project.MessageSender.models.UserSubscriptions;
import com.project.MessageSender.repositories.MessengerRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@RequiredArgsConstructor
public class DefaultUserSubscriptionsDtoConverter {

  private final MessengerRepository messengerRepository;

  public UserSubscriptions defaultConverter() {
    return UserSubscriptions.builder()
        .subscribe(true)
        .typeNews(true)
        .typeError(false)
        .typeUpdate(false)
        .typeWarning(false)
        .messenger(messengerRepository.findByName("mail"))
        .build();
  }
}
